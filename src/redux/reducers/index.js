import { combineReducers } from "redux";

// reducers
import authReducer from "./authReducer";
import venuesReducer from "./venuesReducer";

//kombinasikan semua reducer menjadi satu kesatuan
export default combineReducers({ authReducer, venuesReducer });
