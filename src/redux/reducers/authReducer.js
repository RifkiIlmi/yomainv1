import {
  SAVE_TOKEN,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR
} from "../actionTypes";

// state awal untuk authentication
const initialState = {
  token: "",
  isLoading: false,
  isError: false,
  errMessage: ""
};

//export return pure anonymous function
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SAVE_TOKEN:
      return {
        ...state,
        token: payload
      };
    case LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        token: payload
      };
    case LOGIN_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        errMessage: payload
      };
    default:
      return state;
  }
};
