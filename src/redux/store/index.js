import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

// ambil reducer yang sudah menjadi satu kesatuan
import AppReducers from "../reducers";

// export sebagai store yang besar agar bisa di subscribe dan dispatch
export default createStore(AppReducers, applyMiddleware(thunk));
