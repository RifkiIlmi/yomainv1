import { saveToken, login, venues } from "./authAction";

//export the action to variable
export { saveToken, login, venues };
