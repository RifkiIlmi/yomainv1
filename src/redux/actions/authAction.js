import {
  SAVE_TOKEN,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR
} from "../actionTypes";
import Axios from "axios";

import { baseUrl, token } from "../../constanta";

// action saving user token
export const saveToken = token => {
  return { type: SAVE_TOKEN, payload: token };
};

// action check  login API
export const login = data => {
  return (dispatch, getState) => {
    dispatch({ type: LOGIN_REQUEST });
    Axios({
      method: "POST",
      url: `${baseUrl}/login`,
      headers: {
        Accept: "application/json"
      },
      data: data
    })
      .then(({ data }) => {
        // console.warn("respons login", data);
        //   apabila berhasil
        if (data.success) {
          dispatch({ type: LOGIN_SUCCESS, payload: data.token });
        } else {
          dispatch({ type: LOGIN_ERROR, payload: data.message });
        }
      })
      .catch(err => {
        dispatch({ type: LOGIN_ERROR, payload: err.message });
      });
  };
};

export const venues = data => {
  return (dispatch, getState) => {
    dispatch({ type: LOGIN_REQUEST });
    Axios({
      method: "GET",
      url: `${baseUrl}/venues`,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      },
      data: data
    })
      .then(({ data }) => {
        // console.warn("respons login", data);
        //   apabila berhasil
        if (data.success) {
          dispatch({ type: LOGIN_SUCCESS, payload: data });
        } else {
          dispatch({ type: LOGIN_ERROR, payload: data.message });
        }
      })
      .catch(err => {
        dispatch({ type: LOGIN_ERROR, payload: err.message });
      });
  };
};
