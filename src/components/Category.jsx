import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Ionicons, Entypo, MaterialCommunityIcons } from "@expo/vector-icons";

class Category extends Component {
  render() {
    const { navigation } = this.props;

    return (
      <View style={styles.ctgContainer}>
        <TouchableOpacity
          title="Go to Details"
          onPress={() =>
            navigation.navigate("Detail", {
              itemId: 86,
              otherParam: "anything you want here"
            })
          }
        >
          <View style={styles.categoryBox}>
            <Ionicons name="ios-football" size={52} color="white" />
            <Text style={{ color: "white" }}>Sepak Bola</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.categoryBox}>
            <Entypo name="globe" size={50} color="white" />
            <Text style={{ color: "white" }}>Futsal</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.categoryBox}>
            <Ionicons name="md-basketball" size={50} color="white" />
            <Text style={{ color: "white" }}>Bola Basket</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.categoryBox}>
            <MaterialCommunityIcons name="badminton" size={50} color="white" />
            <Text style={{ color: "white" }}>Badminton</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.categoryBox}>
            <MaterialCommunityIcons name="tennis" size={50} color="white" />
            <Text style={{ color: "white" }}>Bola Tenis</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.categoryBox}>
            <MaterialCommunityIcons name="volleyball" size={50} color="white" />
            <Text style={{ color: "white" }}>Bola Voli</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ctgContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexWrap: "wrap"
  },
  categoryBox: {
    backgroundColor: "#C94C4C",
    width: 100,
    height: 85,
    marginBottom: 12,
    marginLeft: 10,
    marginRight: 10,
    elevation: 5,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default Category;
