import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import {
  Ionicons,
  MaterialIcons,
  Entypo,
  MaterialCommunityIcons
} from "@expo/vector-icons";

// images
import futsal from "../../assets/images/futsal.jpg";

class MatchhCard extends Component {
  render() {
    return (
      <View style={styles.match}>
        <Image style={styles.matchImages} source={futsal} />
        <View style={styles.matchContent}>
          <View style={styles.matchText}>
            <MaterialIcons name="date-range" size={22} color="#C94C4C" />
            <Text
              style={{
                color: "#C94C4C",
                paddingLeft: 6
              }}
            >
              12 Maret 2020
            </Text>
          </View>

          <View style={styles.matchText}>
            <Entypo name="location-pin" size={22} color="#C94C4C" />
            <Text
              style={{
                color: "#C94C4C",
                paddingLeft: 6
              }}
            >
              Jl.Limbungan, Rumbai
            </Text>
          </View>

          <View style={styles.matchText}>
            <MaterialIcons name="access-time" size={22} color="#C94C4C" />
            <Text
              style={{
                color: "#C94C4C",
                paddingLeft: 6
              }}
            >
              08.00 - 10.00
            </Text>
          </View>

          <View style={styles.matchText}>
            <MaterialCommunityIcons name="stadium" size={22} color="#C94C4C" />
            <Text
              style={{
                color: "#C94C4C",
                paddingLeft: 6
              }}
            >
              Lapangan A01
            </Text>
          </View>

          <View style={styles.matchText}>
            <Ionicons name="ios-people" size={22} color="#C94C4C" />
            <Text
              style={{
                color: "#C94C4C",
                paddingLeft: 6
              }}
            >
              6 Orang
            </Text>
          </View>
        </View>
        <View style={styles.matchAction}>
          <TouchableOpacity>
            <Text
              style={{
                paddingRight: 2,
                paddingLeft: 2,
                borderRadius: 5,
                backgroundColor: "#C94C4C",
                color: "white"
              }}
            >
              Detail
            </Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text
              style={{
                paddingRight: 2,
                paddingLeft: 2,
                borderRadius: 5,
                backgroundColor: "#C94C4C",
                color: "white"
              }}
            >
              Ikut
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  match: {
    // backgroundColor: "red",
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    width: 200,
    height: 240,
    alignSelf: "flex-start",
    marginRight: 8,
    marginLeft: 8,
    marginBottom: 8,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  matchImages: {
    width: 185,
    height: 80,
    margin: 3
  },
  matchContent: {
    flexDirection: "column",
    alignItems: "flex-start",
    marginLeft: -20
  },
  matchText: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
    // alignContent: "center"
  },
  matchAction: {
    // backgroundColor: "red",
    width: 180,
    height: 20,
    flex: 1,
    alignSelf: "flex-start",
    marginLeft: 4,
    marginRight: 4,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
});

export default MatchhCard;
