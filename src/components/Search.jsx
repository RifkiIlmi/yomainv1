import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity
} from "react-native";

export class Search extends Component {
  render() {
    return (
      <View style={styles.searchButton}>
        <View style={styles.input}>
          <TextInput placeholderTextColor={"white"} placeholder="Cari . . ." />
        </View>
        <View>
          <TouchableOpacity style={styles.buttonContainer}>
            <Text style={{ color: "white" }}>Search</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchButton: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-end"
  },
  input: {
    backgroundColor: "#C4C4C4",
    alignItems: "flex-start",
    paddingLeft: 15,
    justifyContent: "center",
    borderRadius: 20,
    width: 140,
    height: 30,
    // marginBottom: 15
    marginTop: 15
  },
  buttonContainer: {
    flexDirection: "row",
    // alignItems: "center",
    padding: 5,
    marginTop: 15,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: "#C94C4C",
    borderRadius: 20
    // alignSelf: "center"
  }
});

export default Search;
