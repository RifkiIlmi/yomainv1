import React, { Component } from "react";
import { Picker, StyleSheet, View } from "react-native";

class LocationPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedValue: "Pekanbaru"
    };
  }

  setSelectedValue(val) {
    this.setState({
      selectedValue: val
    });
  }

  render() {
    return (
      <View style={styles.select}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.selectedValue}
          onValueChange={(itemValue, itemIndex) =>
            this.setSelectedValue(itemValue)
          }
        >
          <Picker.Item label="Pekanbaru" value="Pekanbaru" />
          <Picker.Item label="Medan" value="Medan" />
          <Picker.Item label="Banten" value="Banten" />
          <Picker.Item label="Jakarta" value="Jakarta" />
          <Picker.Item label="Palembang" value="Palembang" />
          <Picker.Item label="Bandung" value="Bandung" />
        </Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    backgroundColor: "#C94C4C",
    borderRadius: 25,
    height: 35,
    flexDirection: "row",
    alignItems: "flex-start",
    marginTop: 3
  },
  picker: {
    height: 35,
    width: "100%",
    color: "white",
    marginLeft: 10
  }
});

export default LocationPicker;
