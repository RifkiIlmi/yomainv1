import React, { Component } from "react";
import { SliderBox } from "react-native-image-slider-box";

class Slider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [
        require("../../assets/images/futsal.jpg"),
        require("../../assets/images/badminton-court.jpg"),
        require("../../assets/images/futsal2.jpeg"),
        require("../../assets/images/futsal3.jpg")
      ]
    };
  }

  render() {
    return (
      <SliderBox
        images={this.state.images}
        ImageComponentStyle={{
          borderRadius: 15,
          width: "97%",
          marginTop: 5
        }}
        dotColor="#C94C4C"
        inactiveDotColor="#90A4AE"
        autoplay
        circleLoop
        imageLoadingColor="#C94C4C"
      />
    );
  }
}

export default Slider;
