import React, { Component } from "react";
import { Text } from "react-native";

class TextTitle extends Component {
  render() {
    const { title } = this.props;
    return (
      <>
        <Text
          style={{
            textDecorationLine: "underline",
            fontSize: 18,
            color: "#C94C4C",
            marginLeft: 7,
            marginBottom: 7
          }}
        >
          {title}
        </Text>
      </>
    );
  }
}

export default TextTitle;
