import * as React from "react";
import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { AsyncStorage } from "react-native";

// Screen
import { Login, Daftar, Detail, Home } from "../frames";
// Stack Screen
import StackProfileScreen from "./StackProfileScreen";
import StackHomeScreens from "./StackHomeScreens";

// redux connect
import { connect } from "react-redux";

// actions
import { saveToken } from "../redux/actions";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

class index extends React.Component {
  checkLogin = async () => {
    try {
      let token = await AsyncStorage.getItem("token");
      return token === null ? null : token;
    } catch (error) {
      return null;
    }
  };

  storeData = async value => {
    try {
      await AsyncStorage.setItem("token", value);
    } catch (error) {
      console.warn(error);
    }
  };

  async componentDidUpdate(prevProps) {
    if (this.props.auth.token !== prevProps.auth.token) {
      this.storeData(this.props.auth.token);
    }
  }

  async componentDidMount() {
    let token = await this.checkLogin();
    token !== null ? this.props.saveToken(token) : this.props.saveToken("");
  }

  render() {
    const token = this.props.auth.token;
    console.warn("auth props", this.props.auth.token);

    return token === "" ? (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              headerShown: false
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    ) : (
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === "Home") {
                iconName = focused
                  ? "ios-information-circle"
                  : "ios-information-circle-outline";
              } else if (route.name === "Profile") {
                iconName = focused ? "ios-list-box" : "ios-list";
              }

              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
            }
          })}
          tabBarOptions={{
            activeTintColor: "tomato",
            inactiveTintColor: "gray"
          }}
        >
          <Tab.Screen name="Home" component={StackHomeScreens} />
          <Tab.Screen name="Profile" component={StackProfileScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.authReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);
