import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

// screens
import { Detail, Home } from "../frames";
// redux connect
import { connect } from "react-redux";

// actions
import { saveToken } from "../redux/actions";

const HomeStack = createStackNavigator();

function StackHomeScreens(props) {
  // const token = props.auth.token;

  // console.warn(token);
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false
        }}
      />
      <HomeStack.Screen name="Detail" component={Detail} />
    </HomeStack.Navigator>
  );
}

const mapStateToProps = state => {
  return {
    auth: state.authReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StackHomeScreens);
