import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

// screens
import { Detail, Profile } from "../frames";
// redux connect
import { connect } from "react-redux";

// actions
import { saveToken } from "../redux/actions";

const ProfileStack = createStackNavigator();

function StackProfileScreens() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen name="Profile" component={Profile} />
    </ProfileStack.Navigator>
  );
}

const mapStateToProps = state => {
  return {
    auth: state.authReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StackProfileScreens);
