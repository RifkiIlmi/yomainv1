import React, { useState } from "react";
import {
  Picker,
  StyleSheet,
  TextInput,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { SliderBox } from "react-native-image-slider-box";
import {
  Ionicons,
  MaterialIcons,
  Entypo,
  MaterialCommunityIcons
} from "@expo/vector-icons";

import futsal from "../../assets/images/futsal.jpg";

function Detail({ route, navigation }) {
  const { itemId } = route.params;
  const { otherParam } = route.params;
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Details Screen</Text>
      <Text>itemId: {JSON.stringify(itemId)}</Text>
      <Text>otherParam: {JSON.stringify(otherParam)}</Text>
      {/* <Button
        title="Go to Details... again"
        onPress={() => navigation.push("Detail")}
      /> */}
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}

export default Detail;
