import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

//redux access
//storage
import { AsyncStorage } from "react-native";
// redux connect
import { connect } from "react-redux";
// actions
import { saveToken } from "../redux/actions";

class Profile extends Component {
  logout = () => {
    // this.setState({ token: "" });
    AsyncStorage.removeItem("token");
    this.props.saveToken("");
  };
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => this.logout()}
        >
          <Text style={{ color: "white" }}>Logout</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  buttonContainer: {
    flexDirection: "row",
    // alignItems: "center",
    padding: 5,
    marginTop: 15,
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: "#C94C4C",
    borderRadius: 20
    // alignSelf: "center"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(null, mapDispatchToProps)(Profile);
