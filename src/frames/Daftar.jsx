import React, { Component, useState } from "react";
import { StyleSheet, TextInput, Text, View, Button } from "react-native";

function Daftar() {
  //   const [text, setText] = useState("");
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={{ fontSize: 50 }}>Yo'Main</Text>
      </View>
      <View elevation={10} style={styles.boxText}>
        <Text style={{ fontSize: 20, color: "white" }}>Masuk</Text>
      </View>
      <View elevation={9} style={styles.box}>
        <View style={styles.inputContainer}>
          <View style={styles.input}>
            <TextInput placeholder="Masukkan Email" />
          </View>
          <View style={styles.input}>
            <TextInput placeholder="Masukkan Password" secureTextEntry={true} />
          </View>
          <View style={styles.input}>
            <TextInput
              placeholder="Konfirmasi Password"
              secureTextEntry={true}
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: "column",
            alignItems: "flex-end",
            // flex: 1,
            marginTop: 25,
            right: 10
          }}
        >
          <Button title={"Daftar"} color="#B1CBBB">
            <Text>Daftar</Text>
          </Button>
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          width: 280,
          alignSelf: "center",
          marginTop: 10
        }}
      >
        <Text style={{ color: "grey" }}>Sudah Punya Akun?</Text>
        <Text style={{ color: "#C94C4C" }}>Masuk Disni !</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  title: {
    height: 200,
    // backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center"
  },
  box: {
    // position: "absolute",
    width: 284,
    height: 285,
    backgroundColor: "#C94C4C",
    alignSelf: "center",
    borderRadius: 15
    // alignItems: "center"
    // left: 37,
    // top: 177
  },
  boxText: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#B1CBBB",
    position: "absolute",
    zIndex: 5,
    // marginTop: 10,
    width: 130,
    height: 35,
    left: 115,
    top: 180,
    borderRadius: 15
  },
  inputContainer: {
    // backgroundColor: "grey",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 60
  },
  SmallImage: {
    width: 31,
    margin: 5,
    height: 30
  },
  input: {
    backgroundColor: "white",
    alignItems: "flex-start",
    paddingLeft: 15,
    justifyContent: "center",
    borderRadius: 20,
    width: 236,
    height: 35,
    marginBottom: 15
  },
  otherDaftar: {
    // alignContent: "space-between",
    flexDirection: "row"
  }
});

export default Daftar;
