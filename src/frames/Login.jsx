import React, { Component } from "react";
import { Image, StyleSheet, TextInput, Text, View, Button } from "react-native";

// redux connect
import { connect } from "react-redux";

// actions
import { saveToken, login } from "../redux/actions";

// images/
import gmail from "../../assets/images/gmaillog.png";
import facebook from "../../assets/images/facebooklog.png";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: "",
      email: "",
      password: ""
    };
  }

  handleInput = data => {
    let obj = {
      [data.type]: data.text
    };
    this.setState(obj);
  };

  handleLogin = () => {
    this.props.login(this.state);
  };

  render() {
    // console.warn("tokenlogin:", this.props.auth.token);
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text style={{ fontSize: 50 }}>Yo'Main</Text>
        </View>
        <View elevation={10} style={styles.boxText}>
          <Text style={{ fontSize: 20, color: "white" }}>Masuk</Text>
        </View>
        <View elevation={9} style={styles.box}>
          <View style={styles.inputContainer}>
            <View style={styles.input}>
              <TextInput
                onChangeText={text => this.handleInput({ type: "email", text })}
                placeholder="Masukkan Email atau Username"
              />
            </View>
            <View style={styles.input}>
              <TextInput
                onChangeText={text =>
                  this.handleInput({ type: "password", text })
                }
                placeholder="Masukkan Password"
                secureTextEntry={true}
              />
            </View>
            <Text style={{ color: "white", fontSize: 20 }}>OR</Text>
            <View style={styles.otherLogin}>
              <Image source={gmail} style={styles.SmallImage} />
              <Image source={facebook} style={styles.SmallImage} />
            </View>
          </View>
          <View
            style={{
              flexDirection: "column",
              alignItems: "flex-end",
              right: 10
            }}
          >
            <Button
              onPress={() => this.handleLogin()}
              title="Login"
              color="#B1CBBB"
            ></Button>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            width: 280,
            alignSelf: "center",
            marginTop: 10
          }}
        >
          <Text style={{ color: "grey" }}>Belum Punya Akun?</Text>
          <Text style={{ color: "#C94C4C" }}>Daftar Disni !</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  title: {
    height: 200,
    // backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center"
  },
  box: {
    // position: "absolute",
    width: 284,
    height: 285,
    backgroundColor: "#C94C4C",
    alignSelf: "center",
    borderRadius: 15
    // alignItems: "center"
    // left: 37,
    // top: 177
  },
  boxText: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#B1CBBB",
    position: "absolute",
    zIndex: 5,
    // marginTop: 10,
    width: 130,
    height: 35,
    left: 115,
    top: 180,
    borderRadius: 15
  },
  inputContainer: {
    // backgroundColor: "grey",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 60
  },
  SmallImage: {
    width: 31,
    margin: 5,
    height: 30
  },
  input: {
    backgroundColor: "white",
    alignItems: "flex-start",
    paddingLeft: 15,
    justifyContent: "center",
    borderRadius: 20,
    width: 236,
    height: 35,
    marginBottom: 15
  },
  otherLogin: {
    // alignContent: "space-between",
    flexDirection: "row"
  }
});

const mapStateToProps = state => {
  return {
    auth: state.authReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token)),
    login: data => dispatch(login(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
