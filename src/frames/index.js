import Login from "./Login";
import Daftar from "./Daftar";
import Home from "./Home";
import Detail from "./Detail";
import Profile from "./Profile";

export { Login, Daftar, Home, Detail, Profile };
