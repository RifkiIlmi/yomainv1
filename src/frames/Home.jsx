import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";

//redux access
//storage
import { AsyncStorage } from "react-native";
// redux connect
import { connect } from "react-redux";
// actions
import { saveToken } from "../redux/actions";

//components
import LocationPicker from "../components/LocationPicker";
import Slider from "../components/Slider";
import Search from "../components/Search";
import TextTitle from "../components/TextTitle";
import Category from "../components/Category";
import MatchhCard from "../components/MatchhCard";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: "",
      isLoggedin: false
    };
  }

  _retrieveData = async () => {
    try {
      let token = await AsyncStorage.getItem("token");
      this.setState({ token });
    } catch (error) {
      return "error";
    }
  };

  componentDidMount() {
    this._retrieveData();
  }

  render() {
    // let token = this.state.token;
    // console.warn("home", token);
    console.warn("venes:", this.props.venues);
    return (
      <View style={styles.container}>
        <LocationPicker />
        <ScrollView>
          <Slider />

          <Search />

          <TextTitle title="Lapangan" />

          <Category navigation={this.props.navigation} />

          <TextTitle title="Futsal Match" />
          <ScrollView horizontal={true}>
            <MatchhCard />
            <MatchhCard />
            <MatchhCard />
          </ScrollView>

          <TextTitle title="Basketball Match" />
          <ScrollView horizontal={true}>
            <MatchhCard />
            <MatchhCard />
            <MatchhCard />
          </ScrollView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = state => {
  return {
    venues: state.venuesReducer
  };
};
const mapDispatchToProps = dispatch => {
  return {
    saveToken: token => dispatch(saveToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
