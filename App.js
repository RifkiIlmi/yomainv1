import React from "react";

// redux provider
import { Provider } from "react-redux";

// redux store
import store from "./src/redux/store";

// App Navigation
import AppNavigation from "./src/navigation";

export default function App() {
  return (
    <Provider store={store}>
      <AppNavigation />
    </Provider>
  );
}
